$(document).ready(function(){
	$('.nav__button').on('click',function(){
	  $('.nav').toggleClass('is-expanded');
	  $('body').toggleClass('overflow');
	  $('.nav__items').toggleClass('is-fading-up'); 
	});

	var movementStrength = 25;
	var height = movementStrength / $(window).height();
	var width = movementStrength / $(window).width();

	$("body").mousemove(function(e){
			  var pageX = e.pageX - ($(window).width() / 2);
			  var pageY = e.pageY - ($(window).height() / 2);
			  var newvalueX = width * pageX * -1 - 25;
			  var newvalueY = height * pageY * -1 - 50;
			  $('#mainHeader').css("background-position", newvalueX+"px     "+newvalueY+"px");
			  $('#pricing .row .col-4:nth-child(1) .pricing-item').css("background-position", newvalueX+"px     "+newvalueY+"px");
			  $('#pricing .row .col-4:nth-child(2) .pricing-item').css("background-position", newvalueY+"px     "+newvalueX+"px");
			  $('#pricing .row .col-4:nth-child(3) .pricing-item').css("background-position", newvalueX+"px     "+newvalueY+"px");
			  $('#pricing .row .col-4:nth-child(4) .pricing-item').css("background-position", newvalueY+"px     "+newvalueX+"px");
			  
			  $('#gallery .seq-content .col-3:nth-child(2) .project-photo').css("background-position", newvalueX+"px     "+newvalueY+"px");
			  $('#gallery .seq-content .col-3:nth-child(3) .project-photo').css("background-position", newvalueY+"px     "+newvalueX+"px");
	});

	$('a[href*="#"]')
	  // Remove links that don't actually link to anything
	  .not('[href="#"]')
	  .not('[href="#0"]')
	  .click(function(event) {
		// On-page links
		if (
		  location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
		  && 
		  location.hostname == this.hostname
		) {
		  // Figure out element to scroll to
		  var target = $(this.hash);
		  target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
		  // Does a scroll target exist?
		  if (target.length) {
			// Only prevent default if animation is actually gonna happen
			event.preventDefault();
			$('html, body').animate({
			  scrollTop: target.offset().top
			}, 500, function() {
			  // Callback after animation
			  // Must change focus!
			  var $target = $(target);
			  $target.focus();
			  if ($target.is(":focus")) { // Checking if the target was focused
				return false;
			  } else {
				$target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
				$target.focus(); // Set focus again
			  };
			});
		  }
		}
	  });
	
	$(window).scroll(function() {
		var height = $(window).scrollTop();

		if(height != 0) {
			 $(".scrollToTop").css({
				'visibility':'visible',
				'opacity':'1'
			 });
		} else {
			$(".scrollToTop").css({
				'visibility':'hidden',
				'opacity':'0'
			 });
		}
	});

	$('.pricing-item').mouseenter(function(e){
		e.preventDefault();
		$(this).find(".more-info div").stop().slideDown(500, function(){
			$('.pricing-item').mouseleave(function(){
				$(this).find(".more-info div").stop().slideUp(500);
			});
		});
	}).mouseleave(function(){
		$(this).find(".more-info div").stop().slideUp(500);
	});
	
	$(document).click(function(){
		$(".more-info div").stop().slideUp(500);
	});
	
	


});

(function() {
  var materialForm;

  materialForm = function() {
    return $('.material-field').focus(function() {
      return $(this).closest('.form-group-material').addClass('focused has-value');
    }).focusout(function() {
      return $(this).closest('.form-group-material').removeClass('focused');
    }).blur(function() {
      if (!this.value) {
        $(this).closest('.form-group-material').removeClass('has-value');
      }
      return $(this).closest('.form-group-material').removeClass('focused');
    });
  };

  $(function() {
    return materialForm();
  });

}).call(this);
