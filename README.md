# HTML5/CSS3 One page digital agency template
Screenshots:
* [Screenshot of index page](screenshot-index.png)

## Built With

HTML5/CSS3 <br>
jQuery

## Authors

* **Mantas Salasevicius** - *Developer* - [Portfolio](https://mantas.dev), [Gitlab](https://gitlab.com/mantas.dev)

## Date of development

2018

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE.md](LICENSE.md) file for details